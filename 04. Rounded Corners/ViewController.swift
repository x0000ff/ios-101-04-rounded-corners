//
//  ViewController.swift
//  04. Rounded Corners
//
//  Created by x0000ff on 12/07/15.
//  Copyright (c) 2015 x0000ff. All rights reserved.
//

//#######################################################################
import UIKit

//#######################################################################
class ViewController: UIViewController {

    //#######################################################################
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var roundedView: UIView!

    //#######################################################################
    @IBAction func valueChanged(slider: UISlider) {
        
        let value = slider.value
        
        label.text = "\(value)"
        roundedView.layer.cornerRadius = CGFloat(value)
    }

    //#######################################################################
}
//#######################################################################

